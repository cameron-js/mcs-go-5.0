/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React from "react";
import { NavigationContainer } from '@react-navigation/native'
import HomeScreen from './src/screens/Home/home'
import Ideas from './src/screens/Ideas/Ideas'
import Catalogs from './src/screens/Catalog/catalog'
import Scores from './src/screens/Score/score'
import Grades from './src/screens/Grade/grade'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { NavigationActions } from "react-navigation";
import Idea from "./src/screens/Ideas/Ideas";
import catalog from "./src/screens/Catalog/catalog";
const Tab = createBottomTabNavigator() //creates tab navigation at root, to be used throught the app
export default class AwesomeApp extends React.Component {
  constructor() {
    super();
    this.state = {
      isReady: false
    };
  }
  render() {
    return (
      // NOTE: It seems like only one navigation container is allowed in a screen. 
      <NavigationContainer independent={true}>
        <Tab.Navigator initialRouteName="Home" >
          <Tab.Screen name="Ideas" component={Idea}></Tab.Screen>
          <Tab.Screen name="Catalog" component={catalog}></Tab.Screen>
          <Tab.Screen name="Home" component={HomeScreen}></Tab.Screen>
          <Tab.Screen name="Scores" component={Scores}></Tab.Screen>
          <Tab.Screen name="Grades" component={Grades}></Tab.Screen>
        </Tab.Navigator>
      </NavigationContainer>

    );
  }
}
