import React from 'react';
import { NavigationContainer } from '@react-navigation/native'
import { createDrawerNavigator } from '@react-navigation/drawer'
import { NavigationActions, StackActions } from 'react-navigation'
import Home from "../screens/Home/home"
import PlacesPage from '../screens/places'
import stealMine from '../screens/stealMine'

const Drawer = createDrawerNavigator();
export default function drawerComp(homeRoute) {
    if (homeRoute.name === "PlacesModals") {
        return (
            <NavigationContainer independent={true}>
                <Drawer.Navigator initialRoute={{ title: "Your Screen", index: 0 }}>
                    <Drawer.Screen name={"Places"} component={homeRoute}></Drawer.Screen>
                    <Drawer.Screen name={"Home"} component={Home}></Drawer.Screen>
                </Drawer.Navigator>
            </NavigationContainer>
        )
    }
    return (
        <NavigationContainer independent={true}>
            <Drawer.Navigator initialRoute={{ title: "Your Screen", index: 0 }}>
                <Drawer.Screen name={homeRoute.name} component={homeRoute}></Drawer.Screen>
                <Drawer.Screen name="Places" component={PlacesPage}></Drawer.Screen>
                <Drawer.Screen name="StealMine Channel" component={stealMine}></Drawer.Screen>
            </Drawer.Navigator>
        </NavigationContainer>
    );
}