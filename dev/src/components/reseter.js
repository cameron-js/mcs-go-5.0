import { NavigationActions } from "react-navigation";
import { StackActions } from 'react-navigation'
import {useNavigation } from '@react-navigation/native'

export default function Reset(homeRoute) {         //creates navigation prop, and calls resetAction
    const navigation = useNavigation();
    const resetAction = StackActions.reset({    //resets navigation screen to current screen
        actions: [
          NavigationActions.navigate({ routeName: homeRoute })
        ]
      })
    return (
      navigation.dispatch(resetAction)
    );
  }
