import {Platform } from 'react-native'
import React from 'react'
import Ionicons from 'react-native'
import {Icon} from "native-base"

export function Icons  ({name}) {
    <Ionicons 
    name={`${Platform.OS === "ios" ? "ios" : "md"}-name${name}`}/>
    return (
        <Icon name={name}></Icon>
    )
    
}