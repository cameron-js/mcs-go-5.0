import React from 'react'
import { Button, Text, Left, Right, Title } from 'native-base'
import { useNavigation } from '@react-navigation/native';
import { Icon, Header } from "native-base"
//this is the header component, used throughout the app
export default class MyHeader extends React.Component {
  render() {
    return (
      <Header style={{ backgroundColor: "#989898" }}>
        <Left>
          <GoToHeader>
          </GoToHeader>
        </Left>
        <Title style={{ marginTop: 10 }}>MCS GO 5.0</Title>
        <Right>
        </Right>
      </Header>)
  }
}
function GoToHeader() {
  const navigation = useNavigation();
  return (<Button style={{ backgroundColor: "#15223F" }} rounded onPress={() => navigation.openDrawer()}>
    <Icon style={{ color: "#989898" }} ios="ios-arrow-dropright" android="md-arrow-dropright">
    </Icon>
  </Button>)
}