import React from 'react';
import { WebView } from 'react-native-webview';
import { View } from "native-base";

export default class MiddleSchool extends React.Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <WebView source={{ uri: 'http://data.madisoncity.k12.al.us/documents/Instruction/MCS%20GO%205.0/FlipCatalogs2020-2021/MS_2020-2021/mobile/index.html' }} />
            </View>
        );
    }
}