import React from 'react';
import DrawerComp from '../../components/drawer'
import { WebView } from 'react-native-webview';
import { View, Text } from "native-base";
import Heading from '../../components/header'
export default class score extends React.Component {
  render() {
    return (
      DrawerComp(Scores)
    );
  }
}

class Scores extends React.Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Heading />
        <WebView source={{ html: '<div class="scorestream-widget-container" data-ss_widget_type="vertScoreboard" style="height:100vh;"data-user-widget-id="37963"></div><script async="async" type="text/javascript" src="https://scorestream.com/apiJsCdn/widgets/embed.js"></script>' }} />

      </View>
    );
  }
}
