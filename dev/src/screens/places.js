import React, { useState } from 'react'
import { Root, Header, Content, Text, Button, View, Container, Card, Title } from 'native-base'
import { Modal, StyleSheet, Linking } from 'react-native'
import Heading from '../components/header'
import openMap from 'react-native-open-maps'
import drawerComp from '../components/drawer'
export default class Places extends React.Component {
  render() {
    return (drawerComp(PlacesModals))
  }
}
export class PlacesModals extends React.Component {
  constructor(props) {
    super(props)
    //basic modal data that does not change
    const baseModal = {
      animation: 'slide',
      transparent: false,
      margin: styles.placesButtons,
      cancelText: 'Cancel',
      webText: 'Web',
    }
    //putting modals array in state allows the entire array to be watched for changes, like button presses
    this.state = {
      modals: [
        {
          id: 0,
          url: 'https://www.madisoncity.k12.al.us',
          Text: 'The Annex',
          number: '2564648370',
          lat: 34.694170,
          long: -86.751380,
          ...baseModal
        },
        {
          id: 1,
          url: 'https://www.madisoncity.k12.al.us',
          Text: 'Central Office',
          number: '2564648370',
          lat: 34.687460,
          long: -86.746780,
          ...baseModal

        },
        {
          id: 2,
          url: 'https://www.madisoncity.k12.al.us/Page/2772',
          Text: 'Transportation Depot',
          number: '2567744613',
          lat: 34.687462,
          long: -86.746780,
          ...baseModal
        },
        {
          id: 3,
          url: 'https://www.madisoncity.k12.al.us/Academy',
          Text: 'The Academy',
          number: '2562165313',
          lat: 33.857288,
          long: -86.64742,
          ...baseModal
        },
        {
          id: 4,
          url: 'https://www.madisoncity.k12.al.us/bjhs',
          Text: "Bob Jones High School",
          number: '2567722547',
          lat: '',
          long: '',
          ...baseModal
        },
        {
          id: 5,
          url: 'https://www.madisoncity.k12.al.us/jchs',
          Text: 'James Clemens High School',
          number: '2562165313',
          lat: 33.857288,
          long: -86.647423,
          ...baseModal
        },
        {
          id: 6,
          url: 'https://www.madisoncity.k12.al.us/discovery',
          Text: 'Discovery Middle School',
          number: '2568373735',
          lat: 34.742950,
          long: -86.741140,
          ...baseModal
        },
        {
          id: 7,
          url: 'https://www.madisoncity.k12.al.us/liberty',
          Text: 'Liberty Middle School',
          number: '2564300001',
          lat: 34.6867293,
          long: -86.745732,
          ...baseModal
        },
        {
          id: 8,
          url: 'https://www.madisoncity.k12.al.us/columbia',
          Text: 'Columbia Elementary School',
          number: '2564302751',
          lat: 34.7342308,
          long: -86.766392,
          ...baseModal
        },

        {
          id: 9,
          url: 'https://www.madisoncity.k12.al.us/heritage',
          Text: 'Heritage Elementary School',
          number: '2567722075',
          lat: 34.7208059,
          long: -86.7832102,
          ...baseModal
        },
        {
          id: 10,
          url: 'https://www.madisoncity.k12.al.us/horizon',
          Text: 'Horizon Elementary School',
          number: '2568248106',
          lat: 34.691723291692085,
          long: 86.71352547757664,
          ...baseModal
        },
        {
          id: 11,
          url: 'https://www.madisoncity.k12.al.us/madison',
          Text: 'Madison Elementary School',
          number: '2567729255',
          lat: 34.6978166,
          long: -86.7503253,
          ...baseModal
        },
        {
          id: 12,
          url: 'https://www.madisoncity.k12.al.us/millcreek',
          Text: 'Mill Creek Elementary School',
          number: '2567744690',
          lat: 34.7013918,
          long: -86.7719307,
          ...baseModal
        },
        {
          id: 13,
          url: 'https://www.madisoncity.k12.al.us/rainbow',
          Text: 'Rainbow Elementary School',
          number: '2568248106',
          lat: 34.7512434,
          long: -86.7331008,
          ...baseModal
        },
        {
          id: 14,
          url: 'https://www.madisoncity.k12.al.us/westmadison',
          Text: 'West Madison Elementary School',
          number: '2568371189',
          lat: 34.7212879,
          long: -86.7508178,
          ...baseModal
        },
        {
          id: 15,
          url: 'https://www.madisoncity.k12.al.us/prek',
          Text: 'Prek Center',
          number: '2568248080',
          lat: 34.7535074,
          long: -86.7326267,
          ...baseModal
        },
      ],
      //array of modal visibility
      modalVisibility: [
        false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false
      ]
    }
  }




  //opens maps in phone

  render() {
    handleClick = (linkToOpen) => //checks to see if link can be opened, then opens if it can
    {
      Linking.canOpenURL(linkToOpen).then(supported => {
        if (supported) {
          Linking.openURL(linkToOpen);
        }
        else {
          console.log("Don't know how to open URI: " + this.props.url + ":(");
        }
      })
    }

    const self = this //used to refer to current modals state(visibility)
    const setModalVisible = (id, visibility) => {
      const newVisibility = [...self.state.modalVisibility] //this needs to be an array because modals is an array
      newVisibility[id] = visibility
      self.setState({ modalVisibility: newVisibility })
    }
    var modals = requestModals(self.state.modals); //sets modals equal to all modals 
    function requestModals(modals) {
      return (
        modals.map((modal) => {
          return (
            <View >
              <Modal
                animationType={modal.animation}
                transparent={modal.transparent}
                visible={self.state.modalVisibility[modal.id]}
              >
                <View style={modal.margin}>
                  <Title style={{ alignItems: "center" }}>{modal.Text}</Title>
                  <Button style={{ marginTop: 5 }} onPress={() => this.handleClick(modal.url)}><Text>Web</Text></Button>
                  <Button style={{ marginTop: 5 }} onPress={() => Linking.openURL(`tel:${modal.number}`)}><Text>Phone</Text></Button>
                  <Button style={{ marginTop: 5, marginBottom: 5 }} onPress={() => openMap({ latitude: modal.lat, longitude: modal.long })}><Text>Directions</Text></Button>
                  <Button danger onPress={() => setModalVisible(modal.id, false)}><Text>Cancel</Text></Button>
                </View>

              </Modal>
              <Button style={{ marginBottom: 5 }} onPress={() => setModalVisible(modal.id, true)}><Text>{modal.Text}</Text></Button>
            </View >
          )
        })
      )
    }
    return (
      <Root>
        <Heading></Heading>
        <Title style={{ color: 'black', textAlign: "center" }}>All MCS Locations</Title>
        <Content padder>
          {/* displays all modals */}
          {modals}
        </Content>
      </Root >
    );
  }
}
const styles = StyleSheet.create({
  placesButtons: {
    marginTop: 50,
  }
})