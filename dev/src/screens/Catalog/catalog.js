import React from 'react';
import DrawerComp from '../../components/drawer'
import { Icon, View, Button, Body, Text } from "native-base";
import Heading from '../../components/header'
import WebView from 'react-native-webview';
import DropDownItem from 'react-native-drop-down-item'
import { ScrollView } from 'react-native-gesture-handler';
const urlData = ['http://data.madisoncity.k12.al.us/documents/Instruction/MCS%20GO%205.0/FlipCatalogs2020-2021/HS_2020-2021/index.html',
  'http://data.madisoncity.k12.al.us/documents/Instruction/MCS%20GO%205.0/EasyCourseDescriptions2020-2021/HS/index.html',
  'http://data.madisoncity.k12.al.us/documents/Instruction/MCS%20GO%205.0/FlipCatalogs2020-2021/MS_2020-2021/mobile/index.html',
  'http://data.madisoncity.k12.al.us/documents/Instruction/MCS%20GO%205.0/EasyCourseDescriptions2020-2021/MS/index.html'
]
export default class catalog extends React.Component {
  render() {
    return (
      DrawerComp(Catalog) //pulling in drawer with current screen as the initail route

    );
  }
}
class Catalog extends React.Component {
  state = {
    contents: [{
      title: "Full High School Catalog",
      url: urlData[0]
    },
    {
      title: "Easy High School Catalog",
      url: urlData[1]
    },
    {
      title: "Full Middle School Catalog",
      url: urlData[2]
    },
    {
      title: "Easy Middle School Catalog",
      url: urlData[3]
    }]
  }

  render() {
    MyDownIcon = () => <Icon type="FontAwesome" name="home" ></Icon>;
    return (
      <View style={{ flex: 1 }}>
        <Heading></Heading>
        <ScrollView style={{ flex: 1, alignSelf: 'stretch' }}>
          {
            this.state.contents
              ? this.state.contents.map((param, i) => {
                return (
                  <DropDownItem
                    key={i}
                    style={{ marginBottom: 10 }}

                    // visibleImage={<Icon name="arrow_drop_up" size={30}></Icon>}
                    contentVisible={false}
                    header={
                      <View>
                        <Text>
                          {param.title}
                        </Text>

                      </View>
                    }
                  >
                    <View style={{ flex: 1 }}>
                      <WebView source={{ uri: param.url }} style={{ height: 500, width: 400 }}></WebView>
                    </View>
                  </DropDownItem>

                );
              })
              : null
          }
        </ScrollView>
      </View>
    );
  }
}