import React from 'react';
import DrawerComp from '../../components/drawer'
import Heading from '../../components/header'
import { View, Icon, Content, Left, Right, Header, Button, Text, Toast, Root } from "native-base";
import Webview from 'react-native-webview'
export default class about extends React.Component {
  render() {
    return (
      DrawerComp(About)
    );
  }
}
class About extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showToast: false

    };
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Heading></Heading>
        <Webview source={{ uri: "https://mcsinstruction.com" }}></Webview>
      </View>
    );
  }
}