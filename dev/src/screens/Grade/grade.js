import React from 'react';
import DrawerComp from '../../components/drawer'
import { Container, Body, Content, Header, Button, Text, View } from "native-base";
import Heading from '../../components/header'
import WebView from 'react-native-webview';
import { BackHandler, Platform } from 'react-native';

export default class grade extends React.Component {
  render() {
    return (
      DrawerComp(Grade)
    );
  }
}

class Grade extends React.Component {
  webView = {
    canGoBack: false,
    ref: null,
  }
  componentDidMount() {
    if (Platform.OS === 'android') { BackHandler.addEventListener('hardwareBackPress', this.handleBackButton); }
  }
  componentWillUnmount() {
    if (Platform.OS === 'android') { BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton); }
  }
  handleBackButton = () => {
    if (this.webView.canGoBack && this.webView.ref) {
      this.refs[WEBVIEW_REF].goBack();
      return true;
    }
    return false
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Heading></Heading>
        <WebView source={{ uri: "https://sis.madisoncity.k12.al.us" }}
          ref={(webView) => { this.webView.ref = webView; }}
          onNavigationStateChange={(navState) => { this.webView.canGoBack = navState.canGoBack }}></WebView>
      </View>
    );
  }
}