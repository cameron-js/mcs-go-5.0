import React from 'react';
import DrawerComp from '../../components/drawer'
import { Container, Body, Toast, Text, Button, Left, Right, Root } from "native-base";
import { Image } from 'react-native'
import Heading from '../../components/header'
export class Home extends React.Component {
  render() {
    return (
      <Root>
        <Heading></Heading>
        <Body>
          <Image source={require('../../assets/MCSLogo.jpg')}></Image>
          <Button style={{ marginTop: 5 }} onPress={() => Toast.show({
            text: 'This is the MSC GO app, for teachers and student of Madison city Schools!',
            buttonText: 'Cool!'
          })}><Text>About the App</Text></Button>
        </Body>
      </Root >

    );
  }
}

export default class HomeScreen extends React.Component {
  render() {

    return (
      DrawerComp(Home)
    );
  }
}